#include <stdio.h>
#include <stdlib.h>

// Define the structure of the node
struct node {
    int data;
    struct node *link;
};

// Function to create a new node
struct node *CreateNewNode(int data) {
    // Allocate memory space to the new node
    struct node *NewNode = (struct node *)malloc(sizeof(struct node));

    if (NewNode == NULL) {
        printf("ERROR: Failed to allocate memory space!\n");
        exit(1);
    }

    // Enter the data to be stored
    NewNode->data = data;
    NewNode->link = NULL;
    return NewNode;
}

// Function to print the list
void printList(struct node *head) {
    struct node *current = head;
    printf("[");
    while (current != NULL) {
        printf("%d", current->data);
        current = current->link;
        if (current != NULL)
            printf(", "); // print comma if there are more elements
    }
    printf("]\n");
}

// Function to append elements into the list
void append(struct node **head, int num) {
    struct node *current = CreateNewNode(num);
    if (*head == NULL) {
        *head = current;
        return;
    }

    struct node *temp = *head;
    while (temp->link != NULL) {
        temp = temp->link;
    }
    temp->link = current;
}

// Function to prepend
void prepend(struct node **head, int num) {
    struct node *NewNode = CreateNewNode(num);
    NewNode->link = *head;
    *head = NewNode;
}

// Function to delete a node by key
void deleteByKey(struct node **head, int key) {
    // Check if the list is empty
    if (*head == NULL) {
        printf("List is empty.\n");
        return;
    }

    struct node *temp = *head;  // Pointer to traverse the list
    struct node *prev = NULL;   // Pointer to keep track of the previous node

    // If the node to be deleted is the head
    if (temp != NULL && temp->data == key) {
        *head = temp->link;  // Update head to the next node
        free(temp);          // Free memory of the node
        printf("Node with key %d deleted successfully.\n", key);
        return;
    }

    // Traverse the list to find the node to be deleted
    while (temp != NULL && temp->data != key) {
        prev = temp;
        temp = temp->link;
    }

    // If key was not found in the list
    if (temp == NULL) {
        printf("Node with key %d not found.\n", key);
        return;
    }

    // Unlink the node from linked list
    prev->link = temp->link;
    free(temp);  // Free memory of the node

    printf("Node with key %d deleted successfully.\n", key);
}

// Function to delete a node by value
void deleteByValue(struct node **head, int value) {
    // Check if the list is empty
    if (*head == NULL) {
        printf("List is empty.\n");
        return;
    }

    struct node *temp = *head;   // Pointer to traverse the list
    struct node *prev = NULL;    // Pointer to keep track of the previous node

    // If the node to be deleted is the head
    if (temp != NULL && temp->data == value) {
        *head = temp->link;    // Update head to the next node
        free(temp);            // Free memory of the node
        printf("Node with value %d deleted successfully.\n", value);
        return;
    }

    // Traverse the list to find the node to be deleted
    while (temp != NULL && temp->data != value) {
        prev = temp;
        temp = temp->link;
    }

    // If value was not found in the list
    if (temp == NULL) {
        printf("Node with value %d not found.\n", value);
        return;
    }

    // Unlink the node from linked list
    prev->link = temp->link;
    free(temp);  // Free memory of the node

    printf("Node with value %d deleted successfully.\n", value);
}

// Function to insert a node after a given key
void insertAfterKey(struct node **head, int key, int value) {
    // Check if the list is empty
    if (*head == NULL) {
        printf("List is empty.\n");
        return;
    }

    struct node *temp = *head;   // Pointer to traverse the list

    // Traverse the list to find the node with the specified key
    while (temp != NULL && temp->data != key) {
        temp = temp->link;
    }

    // If key was not found in the list
    if (temp == NULL) {
        printf("Node with key %d not found.\n", key);
        return;
    }

    // Create a new node
    struct node *NewNode = CreateNewNode(value);

    // Insert the new node after the node with the specified key
    NewNode->link = temp->link;
    temp->link = NewNode;

    printf("Node with value %d inserted after key %d.\n", value, key);
}



void insertAfterValue(struct node **head, int searchValue, int newValue) {
    // Check if the list is empty
    if (*head == NULL) {
        printf("List is empty.\n");
        return;
    }

    struct node *temp = *head;   // Pointer to traverse the list

    // Traverse the list to find the node with the specified value
    while (temp != NULL && temp->data != searchValue) {
        temp = temp->link;
    }

    // If value was not found in the list
    if (temp == NULL) {
        printf("Node with value %d not found.\n", searchValue);
        return;
    }

    // Create a new node
    struct node *NewNode = CreateNewNode(newValue);

    // Insert the new node after the node with the specified value
    NewNode->link = temp->link;
    temp->link = NewNode;

    printf("Node with value %d inserted after value %d.\n", newValue, searchValue);
}


int main() {
    struct node *head = NULL;
    int choice, data, key, value, searchValue;

    while (1) {
        printf("\nLinked Lists\n");
        printf("1. Print List\n");
        printf("2. Append\n");
        printf("3. Prepend\n");
        printf("4. Delete by Key\n");
        printf("5. Delete by Value\n");
        printf("6. Insert after Key\n");
        printf("7. Insert after Value\n");
        printf("8. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);

        switch (choice) {
            case 1:
                printList(head);
                break;
            case 2:
                printf("Enter data to append: ");
                scanf("%d", &data);
                append(&head, data);
                printf("List after appending: ");
                printList(head);
                break;

            case 3:
                printf("Enter data to prepend:\n ");
                scanf("%d", &data);
                prepend(&head, data);
                break;
            case 4:
                printf("Enter key to delete: \n");
                scanf("%d", &key);
                deleteByKey(&head, key);
                break;
            case 5:
                printf("Enter value to delete:\n ");
                scanf("%d", &value);
                deleteByValue(&head, value);
                break;
            case 6:
                printf("Enter key after which to insert: \n");
                scanf("%d", &key);
                printf("Enter value to insert: \n");
                scanf("%d", &value);
                insertAfterKey(&head, key, value);
                break;
            case 7:
                printf("Enter value after which to insert: \n");
                scanf("%d", &searchValue);
                printf("Enter value to insert:\n ");
                scanf("%d", &value);
                insertAfterValue(&head, searchValue, value);
                break;
            case 8:
                printf("Exiting...\n");
                exit(0);
            default:
                printf("Invalid choice. Please try again.\n");
        }
    }

    return 0;
}
